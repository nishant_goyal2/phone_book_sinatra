require 'bundler/setup'
require 'sinatra/base'

Bundler.require
# pull in the helpers and controllers
Dir.glob('./app/helpers/*.rb').each { |file| require file }
Dir.glob('./app/models/*.rb').each { |file| require file }
Dir.glob('./app/controllers/*.rb').sort.each { |file| require file }

Mongoid.load!("./config/mongoid.yml", :development)

# map the controllers to routes
map = {
  "/" => ApplicationController,
  "/contacts" => ContactController
}

run Rack::URLMap.new map
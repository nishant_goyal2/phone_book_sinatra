class ContactController < ApplicationController

  get '/' do
    content_type :json
    @contacts = Contact.all.to_a.map { |contact| contact.attributes }.to_json
  end

  post '/' do
    content_type :json
    @contact = Contact.new(params)
    if @contact.save
      @contact.attributes.to_json
    else
      {:errors => @contact.errors.full_messages}.to_json
    end
  end

  get '/:id' do
    content_type :json
    @contact = Contact.where(:id => params[:id])
    (@contact.present? ? @contact.first.try(:attributes) : {}).to_json
  end

  put '/:id' do
    content_type :json
    @contact = Contact.where(:id => params[:id]).first
    
    ["name", "numbers"].each do |attribute| 
      @contact.send("#{attribute}=", params[attribute])
    end
    if @contact.save
      @contact.attributes.to_json
    else 
      {:errors => @contact.errors.full_messages}.to_json
    end
  end

  delete '/:id' do
    content_type :json
    @contact = Contact.where(:id => params[:id]).first

    @contact.destroy
    {}.to_json
  end

  post '/search' do
    @contacts = Contact.where(params.first.first => Regexp.new(params.first.last))
    @contacts.map { |contact| contact.attributes }.to_json
  end
end
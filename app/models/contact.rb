class Contact
  include Mongoid::Document

  field :name, type: String
  field :numbers, type: Array
  validates_presence_of :name, :numbers
end